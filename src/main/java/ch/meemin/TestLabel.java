package ch.meemin;

import javax.annotation.PostConstruct;

import com.vaadin.cdi.UIScoped;
import com.vaadin.ui.Label;

@UIScoped
public class TestLabel extends Label {

	@PostConstruct
	public void init() {
		setCaption("foo");
		setValue("bar");
	}
}
