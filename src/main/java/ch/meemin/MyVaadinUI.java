package ch.meemin;

import javax.inject.Inject;

import com.vaadin.cdi.CDIUI;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.Label;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.UI;

@CDIUI("")
@SuppressWarnings("serial")
public class MyVaadinUI extends UI {

	private TabSheet ts = new TabSheet();
	@Inject
	private TestLabel tl;

	@Override
	protected void init(VaadinRequest request) {
		Label label = new Label("Foobar");
		label.setCaption("now select the other");
		ts.addComponent(label);
		ts.addComponent(tl);
		setContent(ts);
	}

}
